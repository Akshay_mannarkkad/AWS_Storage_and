#include "si1145.h"
#include <stdio.h>
extern I2C_HandleTypeDef hi2c1;

uint8_t Si1145_Init(void) {

  uint8_t id = Si1145_read8(SI1145_REG_PARTID);
  if (id != 0x45) {
  //
    return 1; // look for SI1145
  }

  Si1145_Reset();

  /***********************************/
  // enable UVindex measurement coefficients!
  Si1145_write8(SI1145_REG_UCOEFF0, 0x29);
  Si1145_write8(SI1145_REG_UCOEFF1, 0x89);
  Si1145_write8(SI1145_REG_UCOEFF2, 0x02);
  Si1145_write8(SI1145_REG_UCOEFF3, 0x00);

  // enable UV sensor
  Si1145_writeParam(SI1145_PARAM_CHLIST, SI1145_PARAM_CHLIST_ENUV |
  SI1145_PARAM_CHLIST_ENALSIR | SI1145_PARAM_CHLIST_ENALSVIS |
  SI1145_PARAM_CHLIST_ENPS1);
  // enable interrupt on every sample
  Si1145_write8(SI1145_REG_INTCFG, SI1145_REG_INTCFG_INTOE);
  Si1145_write8(SI1145_REG_IRQEN, SI1145_REG_IRQEN_ALSEVERYSAMPLE);

/****************************** Prox Sense 1 */

  // program LED current
  Si1145_write8(SI1145_REG_PSLED21, 0x03); // 20mA for LED 1 only
  Si1145_writeParam(SI1145_PARAM_PS1ADCMUX, SI1145_PARAM_ADCMUX_LARGEIR);
  // prox sensor #1 uses LED #1
  Si1145_writeParam(SI1145_PARAM_PSLED12SEL, SI1145_PARAM_PSLED12SEL_PS1LED1);
  // fastest clocks, clock div 1
  Si1145_writeParam(SI1145_PARAM_PSADCGAIN, 0);
  // take 511 clocks to measure
  Si1145_writeParam(SI1145_PARAM_PSADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK);
  // in prox mode, high range
  Si1145_writeParam(SI1145_PARAM_PSADCMISC, SI1145_PARAM_PSADCMISC_RANGE|
    SI1145_PARAM_PSADCMISC_PSMODE);

  Si1145_writeParam(SI1145_PARAM_ALSIRADCMUX, SI1145_PARAM_ADCMUX_SMALLIR);
  // fastest clocks, clock div 1
  Si1145_writeParam(SI1145_PARAM_ALSIRADCGAIN, 0);
  // take 511 clocks to measure
  Si1145_writeParam(SI1145_PARAM_ALSIRADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK);
  // in high range mode
  Si1145_writeParam(SI1145_PARAM_ALSIRADCMISC, SI1145_PARAM_ALSIRADCMISC_RANGE);

  // fastest clocks, clock div 1
  Si1145_writeParam(SI1145_PARAM_ALSVISADCGAIN, 0);
  // take 511 clocks to measure
  Si1145_writeParam(SI1145_PARAM_ALSVISADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK);
  // in high range mode (not normal signal)
  Si1145_writeParam(SI1145_PARAM_ALSVISADCMISC, SI1145_PARAM_ALSVISADCMISC_VISRANGE);

/************************/

  // measurement rate for auto
  Si1145_write8(SI1145_REG_MEASRATE0, 0xFF); // 255 * 31.25uS = 8ms

  // auto run
  Si1145_write8(SI1145_REG_COMMAND, SI1145_PSALS_AUTO);
  //

  return 0;
}

void Si1145_Reset() {
  Si1145_write8(SI1145_REG_MEASRATE0, 0);
  Si1145_write8(SI1145_REG_MEASRATE1, 0);
  Si1145_write8(SI1145_REG_IRQEN, 0);
  Si1145_write8(SI1145_REG_IRQMODE1, 0);
  Si1145_write8(SI1145_REG_IRQMODE2, 0);
  Si1145_write8(SI1145_REG_INTCFG, 0);
  Si1145_write8(SI1145_REG_IRQSTAT, 0xFF);

  Si1145_write8(SI1145_REG_COMMAND, SI1145_RESET);
  HAL_Delay(10);
  Si1145_write8(SI1145_REG_HWKEY, 0x17);

  HAL_Delay(10);
}

// returns the UV index * 100 (divide by 100 to get the index)
uint16_t Si1145_readUV(void) {
 return Si1145_read16(0x2C);
}

// returns visible+IR light levels
uint16_t Si1145_readVisible(void) {
 return Si1145_read16(0x22);
}

// returns IR light levels
uint16_t Si1145_readIR(void) {
 return Si1145_read16(0x24);
}

// returns "Proximity" - assumes an IR LED is attached to LED
uint16_t Si1145_readProx(void) {
 return Si1145_read16(0x26);
}

/*********************************************************************/

uint8_t Si1145_writeParam(uint8_t p, uint8_t v) {

  Si1145_write8(SI1145_REG_PARAMWR, v);
  Si1145_write8(SI1145_REG_COMMAND, p | SI1145_PARAM_SET);

  return Si1145_read8(SI1145_REG_PARAMRD);
}

uint8_t Si1145_readParam(uint8_t p) {
  Si1145_write8(SI1145_REG_COMMAND, p | SI1145_PARAM_QUERY);
  return Si1145_read8(SI1145_REG_PARAMRD);
}

/*********************************************************************/

uint8_t Si1145_read8(uint8_t addr)  {

  uint8_t read_byte;

  HAL_I2C_Mem_Read(&hi2c1, SI1145_I2C_ADDRESS, addr, 1, &read_byte, 1, 10);

  return read_byte;
}

uint16_t Si1145_read16(uint8_t addr) {

  uint16_t value;
  uint8_t read_byte[2];

  HAL_I2C_Mem_Read(&hi2c1, SI1145_I2C_ADDRESS, addr, 1, read_byte, 2, 10);

  value = (read_byte[0] | ((uint16_t)(read_byte[1])<<8));

  return value;
}

void Si1145_write8(uint8_t reg, uint8_t val) {

  HAL_I2C_Mem_Write(&hi2c1, SI1145_I2C_ADDRESS, reg, 1, &val, 1, 10);
}
