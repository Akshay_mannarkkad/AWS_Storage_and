################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../App/Weather_Station/i2c.c \
../App/Weather_Station/si1145.c \
../App/Weather_Station/weather_station.c 

OBJS += \
./App/Weather_Station/i2c.o \
./App/Weather_Station/si1145.o \
./App/Weather_Station/weather_station.o 

C_DEPS += \
./App/Weather_Station/i2c.d \
./App/Weather_Station/si1145.d \
./App/Weather_Station/weather_station.d 


# Each subdirectory must supply rules for building sources it contributes
App/Weather_Station/i2c.o: ../App/Weather_Station/i2c.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/BME_280 -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../App/Core/Inc -I../App/LoRaWAN/inc -I../App/Weather_Station -I../App/Startup -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/BSP/CMWX1ZZABZ-0xx -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"App/Weather_Station/i2c.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
App/Weather_Station/si1145.o: ../App/Weather_Station/si1145.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/BME_280 -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../App/Core/Inc -I../App/LoRaWAN/inc -I../App/Weather_Station -I../App/Startup -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/BSP/CMWX1ZZABZ-0xx -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"App/Weather_Station/si1145.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
App/Weather_Station/weather_station.o: ../App/Weather_Station/weather_station.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/BME_280 -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../App/Core/Inc -I../App/LoRaWAN/inc -I../App/Weather_Station -I../App/Startup -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/BSP/CMWX1ZZABZ-0xx -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"App/Weather_Station/weather_station.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

